'''This file contains all the views of perform_QC_run'''
#python imports
import os
import subprocess
import re
#django imports
from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
#project imports
from projects_manager_app.settings import PATH, PATH_SCRIPT, BASE_DIR

# Create your views here.
@login_required(login_url="/user_management/login/")
def select_project(request):
    '''select_project view, fills the project_list, arr_files_subfiles and fastq_list
    with the contain of the project directory and his subcontent
    render the perform_QC_run/select_project_and_options.html with this information'''
    arr_files_subfiles = []
    fastq_list = []
    project_list = list_projects()
    for project in project_list:
        file_subfile = []
        fastq_list = list_fastq_files(".fastq", project + "/fastq")
        fastq_list.extend(list_fastq_files(".fq", project + "/fastq"))
        bed_list = list_bed_files(".bed", project + "/INPUTS")
        file_subfile = [[project], fastq_list, bed_list]
        arr_files_subfiles.append(file_subfile)
    args = {"projects":project_list, "arr_files_subfiles":arr_files_subfiles}
    return render(request, "perform_QC_run/select_project_and_options.html", args)

@login_required(login_url="/user_management/login/")
def loading(request):
    '''loading view, this view get all the requests from the select_project view
    calls the pipeline (its a long long process), but after this process it
    redirects to the /history/projects view'''
    if request.method=="POST":
        #vars
        project_selected = request.POST.get("sel_project")
        fastqsR1 = request.POST.get("sel_R1_fastqs")
        fastqsR2 = request.POST.get("sel_R2_fastqs")
        bed = request.POST.get("sel_bed")
        #genome = request.POST.get("sel_genome")
        path_project = PATH + project_selected
        path_bed = path_project + "/INPUTS/" + bed
        path_multiqc = BASE_DIR + "/history/templates/history/projects/" + project_selected
        # print("/bin/bash " + PATH_SCRIPT + " " + path_project + " " + path_bed + " " + fastqsR1 + " " + fastqsR2 + " "  + path_multiqc)
        print("screen -dm -S medel /bin/bash " + PATH_SCRIPT + " " + path_project + " " + path_bed + " " + fastqsR1 + " " + fastqsR2 + " "  + path_multiqc)
        subprocess.call("screen -dm -S medel /bin/bash " + PATH_SCRIPT + " " + path_project + " " + path_bed + " " + fastqsR1 + " " + fastqsR2 + " "  + path_multiqc, shell=True)
        # subprocess.call("/bin/bash " + PATH_SCRIPT + " " + path_project + " " + path_bed + " " + fastqsR1 + " " + fastqsR2 + " "  + path_multiqc, shell=True)
        return redirect('/history/projects')


# functions
def list_projects() -> list:
    '''list_projects function, checks if the directories of the project match a pattern,
    return a list of projects'''
    output = subprocess.Popen("ls " + PATH + " | grep -P '[a-zA-Z]+_[0-9]{4}_'", shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    output_str = output_str.replace("b'","")
    output_str = output_str.replace("\\n'","") 
    file_list = output_str.split("\\n")
    return file_list

def list_fastq_files(type_file: str, subpath: str) -> list:
    '''list_fastq_files function, checks the type_file and the directory,
    this function is different from the list_bed_files because fastq files are normally compressed but may not be
    returns a list of matching files (in this case fastq files)'''
    files = []
    output = subprocess.Popen("ls " + PATH + subpath, shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    output_str = output_str.replace("b'","")
    output_str = output_str.replace("\\n'","") 
    file_list = output_str.split("\\n")
    for file_ in file_list: 
        if (file_.find(type_file) != -1) :
            files.append(file_)
    return files

def list_bed_files(type_file: str, subpath: str) -> list:
    '''list_fastq_files function, checks the type_file and the directory,
    this function is different from the list_fastq_files because in the INPUTS directory
    there is a .bed file and another .bed.list among other files
    returns a list of matching files (in this case bed files)'''
    files = []
    output = subprocess.Popen("ls " + PATH + subpath, shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    output_str = output_str.replace("b'","")
    output_str = output_str.replace("\\n'","") 
    file_list = output_str.split("\\n")
    for file_ in file_list: 
        if (file_.endswith(type_file)) :
            files.append(file_)
    return files


