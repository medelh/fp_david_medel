/*Javascript associated to select_project_and_options.html */
$(document).ready(function () {
    //general var
    arr_files_subfiles = [];
    count_fastq_files = 0;

    //call onload() function
    onload();

    /**
     * select_project, change action, calls other functions
     */
    $("#select_project").change(function () {
        fill_fastq_and_bed_select();
        remove_fastq_selected();
        show_hide_errors();
        fill_post_data();
        enable_disable_btn_submit();
    });

    /**
     * btnAdd, on click, move the selected files of select_fastq_list to select_fastq_list_selected
     * once this call other functions
     */
    $("#btnAdd").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list")[0].length - 1; i >= 0; i--) {
            if ($("#select_fastq_list")[0][i].selected) {
                $("#select_fastq_list")[0][i].selected = false;
                $("#select_fastq_list_selected").append($("#select_fastq_list")[0][i]);
            }
        }
        fill_post_data();
        enable_disable_btn_submit();
        fill_count_fastq_files();
    });

    /**
     * btnRemove, on click, move the selected files of select_fastq_list_selected to select_fastq_list
     * once this call other functions
     */
    $("#btnRemove").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list_selected")[0].length - 1; i >= 0; i--) {
            if ($("#select_fastq_list_selected")[0][i].selected) {
                $("#select_fastq_list_selected")[0][i].selected = false;
                $("#select_fastq_list").append($("#select_fastq_list_selected")[0][i]);
            }
        }
        fill_post_data();
        enable_disable_btn_submit();
        fill_count_fastq_files();
    });

    /**
     * btnSelectAll, on click, move all files from select_fastq_list to select_fastq_list_selected
     * once this call other functions
     */
    $("#btnSelectAll").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list")[0].length - 1; i >= 0; i--) {
            $("#select_fastq_list")[0][i].selected = false;
            $("#select_fastq_list_selected").append($("#select_fastq_list")[0][i]);
        }
        fill_post_data();
        enable_disable_btn_submit();
        fill_count_fastq_files();
    });

    /**
     * btnUnselectAll, on click, move all files from select_fastq_list_selected to select_fastq_list
     * once this call other functions
     */
    $("#btnUnselectAll").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_fastq_list_selected")[0].length - 1; i >= 0; i--) {
            $("#select_fastq_list_selected")[0][i].selected = false;
            $("#select_fastq_list").append($("#select_fastq_list_selected")[0][i]);
        }
        fill_post_data();
        enable_disable_btn_submit();
        fill_count_fastq_files();
    })

    /**
     * select_bed_list, on change action, calls fill_post_data() function
     */
    $("#select_bed_list").change(function () {
        fill_post_data()
    });

    /**
     * select_genome, on change action, calls fill_post_data() function
     */
    $("#select_genome").change(function () {
        fill_post_data()
    });

    /**
     * btn_submit, on change action, calls fill_post_data() function
     */
    $("#btn_submit").click(function (event) {
        $("#form").hide();
        $("#loading").show();
    })

});

/**
 * @name onload()
 * @description hide some elements, and call other functions
 */
function onload() {
    $("#str_files_subfiles").hide();
    $('.hide_option_selected').hide();
    transform_str_files_subfiles_to_arr();
    fill_fastq_and_bed_select();
    show_hide_errors();
    fill_post_data();
    enable_disable_btn_submit();
    hide_loading();
}

/**
 * @name transform_str_files_subfiles_to_arr()
 * @description function that transforms the str_files_subfiles to array
 */
function transform_str_files_subfiles_to_arr() {
    str_files_subfiles = $("#str_files_subfiles").text()
    str_files_subfiles = str_files_subfiles.replace("[[", "")
    str_files_subfiles = str_files_subfiles.replace("]]]", "")
    str_files_subfiles = str_files_subfiles.replace(/'/g, "")
    str_files_subfiles = str_files_subfiles.replace(/ /g, "")
    str_files_subfiles = str_files_subfiles.split("]],[")
    for (let i = 0; i < str_files_subfiles.length; i++) {
        arr_aux = str_files_subfiles[i].replace("[", "")
        arr_aux = arr_aux.split("],[")
        arr_aux_file_subfile = []
        for (let j = 0; j < arr_aux.length; j++) {
            arr_aux_file_subfile.push(arr_aux[j].split(","))
        }
        arr_files_subfiles.push(arr_aux_file_subfile)
    }
}

/**
 * @name fill_fastq_and_bed_select()
 * @description function that fills the diferents selects with the options of the arr_files_subfiles
 */
function fill_fastq_and_bed_select(){
    $('#select_fastq_list').find('option').remove();
    for (let i = 0; i < arr_files_subfiles.length; i++) {
        if ($("#select_project option:selected")[0].value == i) {
            for (let j = 0; j < arr_files_subfiles[i][1].length; j++) {
                if(arr_files_subfiles[i][1][j] != ""){
                    var o = new Option(arr_files_subfiles[i][1][j], arr_files_subfiles[i][1][j]);
                    $("#select_fastq_list").append(o);
                }
            }
            $('#select_bed_list').find('option').remove();
            for (let j = 0; j < arr_files_subfiles[i][2].length; j++) {
                if(arr_files_subfiles[i][2][j] != ""){
                    var o = new Option(arr_files_subfiles[i][2][j], arr_files_subfiles[i][2][j]);
                    $("#select_bed_list").append(o);
                }
            }
        }
    }
}

/**
 * @name remove_fastq_selected()
 * @description function that removes the .fastq selected when change the project
 */
function remove_fastq_selected(){
    $('#select_fastq_list_selected').find('option').remove();
}

/**
 * @name show_hide_errors()
 * function that shows/hide some elements if the list of .fastqs and .bed files is void
 */
function show_hide_errors(){
    if ($("#select_fastq_list")[0].length == 0){
        $("#div_fastqs").hide();
        $("#div_error_fastqs").show();
    }else{
        $("#div_fastqs").show();
        $("#div_error_fastqs").hide();
    }
    if ($("#select_bed_list")[0].length == 0){
        $("#div_bed").hide();
        $("#div_error_bed").show();
    }else{
        $("#div_bed").show();
        $("#div_error_bed").hide();
    }
}

/**
 * @name fill_post_data()
 * @description function that fills the diferent spans with the data selected
 * it is not a very orthodox method but i didn't find a way to send all the elements
 * of a multiselect as a response from a submit
 */
function fill_post_data(){
    //fill input project selected
    $('#input_project_selected').val($('#select_project option:selected').text());
    //fill input fastq list selected
    var optionR1Values = [];
    var optionR2Values = [];
    $('#select_fastq_list_selected option').each(function() {
        if($(this).val().search("R1") >= 0){
            optionR1Values.push($(this).val());
        }else if($(this).val().search("R2") >= 0){
            optionR2Values.push($(this).val());
        }
    });
    $('#input_fastq_R1_selected').val(optionR1Values.toString());
    $('#input_fastq_R2_selected').val(optionR2Values.toString());
    //fill input bed selected
    $('#input_bed_selected').val($('#select_bed_list option:selected').text());
    //fill input genome selected
    $('#input_genome_selected').val($('#select_genome option:selected').text());
    //fill input paralleled processes selected
    $('#input_num_pp_selected').val($('#select_parallel_processes option:selected').text());
    //fill input user selected
    $('#input_user_selected').val($('#select_user option:selected').text());
}

/**
 * @name fill_count_fastq_files()
 * @description function that fills the aprox_time var with the calculated processed time
 */
function fill_count_fastq_files(){
    count_fastq_files = $("#select_fastq_list_selected")[0].length;
    var expected_time = count_fastq_files * 60;
    $('#aprox_time').text("This process will take round: " + expected_time + " minutes");
}

/**
 * @name enable_disable_btn_submit()
 * @description enable/disable button submit if nothing to submit
 */
function enable_disable_btn_submit(){
    if ($("#select_fastq_list_selected")[0].length == 0){
        $("#btn_submit").prop('disabled', true);
    }else{
        $("#btn_submit").prop('disabled', false);
    }
}

/**
 * @name hide_loading()
 * @description function that hides the div loading
 */
function hide_loading(){
    $("#loading").hide();
}