'''This file contains all the urls in perform_QC_run'''
from django.urls import path, re_path
from . import views

urlpatterns = [
    re_path(r'^select_project$', views.select_project),
    re_path(r'^loading$', views.loading)
]