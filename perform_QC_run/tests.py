import re
from django.test import TestCase
from . import views
from projects_manager_app.settings import PATH

# Create your tests here.

class perform_qc(TestCase):
    def test_valid_list_projects(self):
        'est to check the function if all projects match the pattern'
        pattern = re.compile("[a-zA-Z]+_[0-9]{4}_")
        assert all(pattern.match(project) for project in views.list_projects())
    
    def test_not_valid_list_projects(self):
        'Test to check the function if all projects dont match the pattern'
        pattern = re.compile("[a-zA-Z]+_[0-9]{5}_")
        assert (all(pattern.match(project) for project in views.list_projects())!=True)
    
    def test_valid_list_fastq_files(self):
        'Test to check if any of the projects contains fastq files'
        lp = views.list_projects()
        type_file = "fq"
        flag = False
        for p in lp:
            for fastq in views.list_fastq_files(type_file, p + "/fastq"):
                if fastq.find(type_file):
                    flag = True
                    break
            if flag == True:
                break
        assert flag
        
    def test_not_valid_list_fastq_files(self):
        'Test to check if any of the projects contains csv files'
        lp = views.list_projects()
        type_file = ".csv"
        flag = False
        for p in lp:
            for fastq in views.list_fastq_files(type_file, p + "/fastq"):
                if fastq.find(type_file):
                    flag = True
                    break
            if flag == True:
                break
        assert (flag == False)

    def test_valid_list_bed_files(self):
        'Test to check if any of the projects contains .bed files'
        lp = views.list_projects()
        type_file = ".bed"
        flag = False
        for p in lp:
            for fastq in views.list_fastq_files(type_file, p + "/INPUTS"):
                if fastq.endswith(type_file):
                    flag = True
                    break
            if flag == True:
                break
        assert flag