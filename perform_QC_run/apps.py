from django.apps import AppConfig


class PerformQcRunConfig(AppConfig):
    name = 'perform_QC_run'
