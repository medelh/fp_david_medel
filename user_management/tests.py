import re
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django_utils.templatetags import user_groups

# Create your tests here.
class YourTestCase(TestCase):
    def test_success_login(self):
        'Test if the user login successfully'
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        c = Client()
        assert c.login(username='testuser', password='12345')
    
    def test_not_success_login(self):
        'Test if the user login not successfully'
        user = User.objects.create(username='testuser')
        user.set_password('12345')
        user.save()
        c = Client()
        assert (c.login(username='testuser', password='54321') == False)

    def test_redirect_logout(self):
        'Test if the redirects works successfully'
        c = Client()
        response = c.get('/user_management/logout/', follow=True)
        redirect = [('/user_management/login', 302), ('/user_management/login/', 301)]
        assert response.redirect_chain == redirect
    
    def test_wrong_redirect_logout(self):
        'Test if the redirects works successfully'
        c = Client()
        response = c.get('/user_management/logout/', follow=True)
        redirect = [('/home', 302), ('/home/', 301)]
        assert response.redirect_chain != redirect