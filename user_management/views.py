'''This file contains all the views of user_management'''
#django imports
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
#forms import
from .forms import CreateUserForm

# Create your views here.
def register_page(request):
    '''register_page view, returns to /home if authentication is succesfully,
    else call the form method, redirect to login if all fields are correct,
    render the register.html displaying messages'''
    if request.user.is_authenticated:
        return redirect('/home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account created for ' + user)
                return redirect('/user_management/login')
        context = {'form':form}
        return render(request, 'user_management/register.html', context)

def login_page(request):
    '''login_page view, returns to /home if authentication is succesfully,
    checks that the user and password are valids,
    render some errors message if theres a problem with login,
    redirect to /home page if login success'''
    if request.user.is_authenticated:
        return redirect('/home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/home')
            else:
                messages.info(request, 'Username OR password is incorrect')
        context = {}
        return render(request, 'user_management/login.html', context)

def logout_user(request):
    '''logout_user view, logout the user and redirects to login page'''
    logout(request)
    return redirect('/user_management/login')
