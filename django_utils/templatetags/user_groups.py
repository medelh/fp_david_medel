'''this file contains a method that checks if the user belongs a group or not'''
from django import template
from django.contrib.auth.models import Group

register = template.Library()

@register.filter(name='has_group')
def has_group(user, group_name):
    'has_group function, returns true if user belongs a certain     group false otherwise'
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False
