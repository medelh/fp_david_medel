# 1) DIRECTORIES 
- db.sqlite3 (file that contains the data of the database)
- manage.py (main of the program, file that allows to run the application)
- django_utils: (verify group used in FR4, FR5, FR6)
    * \_\_init\_\_.py
    * user_groups.py (function that contains a function that checks if the user belongs to a group)
- history (history app) (FR6, FR7, FR8, FR9):
    * (admin.py, apps.py, \_\_init\_\_.py, migrations/, models.py, \_\_pycache\_\_/, tests.py)
    * static (css, js, images)
    * templates (html files)
    * urls.py (urls to the views)
    * views.py (views of history)
- perform\_QC\_run (perform\_QC\_run app) (FR5):
    * (admin.py, apps.py, \_\_init\_\_.py, migrations/, models.py, \_\_pycache\_\_/)
    * static (css, js, images)
    * templates (html files)
    * tests.py (file that contains some tests)
    * urls.py (urls to the view)
    * views.py (views of perform\_QC\_run)
- __projects\_manager\_app__ (FR4, FR3 (is called here but is in the default Django configuration in my case /home/alumne/anaconda3/envs/bio/lib/python3.7/site-packages/django))
    * asgi.py (file that allows to deploy the app in Asynchronous Server Gateway Interface)
    * \_\_init \_\_.py (\_\_init\_\_.py are used to mark directories on disk as Python package directorie)
    * \_\_pycache\_\_
    * settings.py (file that contains all the settings of the application)
    * static (css, js, images)
    * templates (html files)
    * urls.py (urls to views)
    * views.py (views of projects\_manager\_app)
    * wsgi.py (file that allows to deploy the app in Web Server Gateway Interface)
- user\_management (FR1, FR2)
    * (admin.py, apps.py, \_\_init\_\_.py, migrations/, models.py, \_\_pycache\_\_/)
    * forms.py (user form)
    * static (css, js, images)
    * templates (html files)
    * tests.py (file that contains some tests)
    * urls.py (urls to the view)
    * views.py (views of user\_management)
- README.md

# 2) MAIN FUNCTION
The main function is located on the __manage.py__ file

# 3) INSTALLATION
- Django installation
    * install python3
    * install SQlite3
    * install Django
- other software to run the pipeline
    * fastqc
    * picard.jar
    * samtools
    * qualimap
    * bwa
    * multiqc
    * GATK (Mutect2)
    * GATK (VariantFiltration)


# 4) COMPILATION
not yet in production, __how to django with apache and mod\_wsgi__:

<https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/modwsgi/>

how do I do for the moment:
1. modify settings to allow all hosts
2. download the project from gitlab (in the server)
3. go to the root of the project
4. $ python3 manage.py runserver 0.0.0.0:8000
5. open the client browser and introduce the <ip of the server>:8000

# 5) MANUALS
the manuals folder are in a folder in the root of the project