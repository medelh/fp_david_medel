'''This file contains all the views of history'''
#python imports
import subprocess
import re
#django imports
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
#project imports
from projects_manager_app.settings import PATH, URL_SERVER

# Create your views here.
@login_required(login_url="/usermanagement/login/")
def history(request):
    '''history view, checks the name and group of the user and render the history/projects.html
    with the information of the user and the information of the diferents projects'''
    qs_groups = request.user.groups.values_list('name', flat=True) # QuerySet Object
    groups = list(qs_groups) #list object
    username = request.user.username
    projects_str = ""
    if 'technicians' in groups:
        projects_str = ls_projects("") #listall projects
    else:
        projects_str = ls_projects(username) #grep by user
    #regex to get the different values
    reg = r"(\w{3}) *([1-3]?\d) *(\d{2}:\d{2}) *((\w*)_(\d{4}).*?)\\n"
    pat = re.compile(reg)
    info_projects = pat.findall(projects_str)
    args = {"username":username, "info_projects":info_projects}
    return render(request, 'history/projects.html', args)


@login_required(login_url="/usermanagement/login/")
def multiqc(request):
    'multiqc view, get the name of the project from the path and render the history/multiQC.html with the correct multiQC'
    path = request.path
    project_name = path[17:]
    path_multiqc = "history/projects/" + project_name + "/multiqc_report.html"
    args = {'project_name':project_name, 'path_multiqc':path_multiqc}
    return render(request, 'history/multiQC.html', args)


@login_required(login_url="/usermanagement/login/")
def bams(request):
    '''bams view, get the name of the project from the path, makes a list of the bam files in this project
    and render the history/BAM.html with the user information and this information'''
    path = request.path
    project_name = path[13:]
    bams_str = ls_bams(project_name)
    reg = r"([^\']*?)\\n"
    pat = re.compile(reg)
    bam_list = pat.findall(bams_str)
    args = {'project_name':project_name, 'bam_list':bam_list, 'url_server':URL_SERVER}
    return render(request, 'history/BAM.html', args)


@login_required(login_url="/usermanagement/login/")
def vcfs(request):
    '''vcfs view, get the name of the project from the path, makes a list of the vcf files,
    separate it in filtered and nonfiltered, then render the history/VCF.html with the user information and this information'''
    path = request.path
    project_name = path[13:]
    vcfs_str = ls_vcfs(project_name)
    reg = r"([^\']*?)\\n"
    pat = re.compile(reg)
    vcf_list = pat.findall(vcfs_str)
    nonfiltered_list = []
    filtered_list = []
    for i in range(len(vcf_list)):
        if vcf_list[i].find("_f.") >= 0:
            filtered_list.append(vcf_list[i])
        else:
            nonfiltered_list.append(vcf_list[i])
    args = {'project_name':project_name, 'nonfilteredvcf_list':nonfiltered_list, 'filteredvcf_list':filtered_list, 'url_server':URL_SERVER}
    return render(request, 'history/VCF.html', args)


# functions
def ls_projects(username: str) -> str:
    '''ls_projects function list the projects (list all if technician, list own if nontechnician),
    grep depending on this project contain a multiqc file (project runned)
    and grep matching a the pattern of the projectname
    returns the information as string'''
    project_with_multiqc = get_project_with_multiqc_file()
    if username == "":
        output = subprocess.Popen("ls -l " + PATH + " | grep -P '" + project_with_multiqc + "' | grep -P '[a-zA-Z]+_[0-9]{4}_'", shell=True, stdout=subprocess.PIPE)
    else:
        output = subprocess.Popen("ls -l " + PATH + " | grep -P '" + project_with_multiqc + "' | grep -P '[a-zA-Z]+_[0-9]{4}_'" + " | grep " + username + "_", shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    return output_str


def ls_bams(project_name: str) -> str:
    '''ls_bams function list all the .bam files in the given directory, return the result as string'''
    output = subprocess.Popen("ls " + PATH + project_name + "/bam" + " | grep .bam", shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    return output_str

def ls_vcfs(project_name: str) -> str:
    '''ls_vcfs function list all the .vcf files in the given directory, return the result as string'''
    output = subprocess.Popen("ls " + PATH + "/" + project_name + "/vcf" + " | grep .vcf", shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    return output_str

def get_project_with_multiqc_file() -> str:
    '''get_project_with_multiqc_file function, this function checks if each project contains a multiqc_report.html file
    this is the condition that the pipeline has been run, return an string of each file separated with |'''
    output = subprocess.Popen("find " + PATH + " -name 'multiqc_report.html' -printf '%h\n'", shell=True, stdout=subprocess.PIPE)
    output_bytes = output.communicate()[0]
    output_str = str(output_bytes)
    reg = rf"{PATH}(.*?)\\"
    pat = re.compile(reg)
    list_projects_with_multiqc = pat.findall(output_str)
    str_projects_with_multiqc = "|".join(list_projects_with_multiqc)
    return str_projects_with_multiqc
