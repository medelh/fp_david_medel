/*Javascript associated to VCF.html */
$(document).ready(function () {

    enable_disable_btn_download_nf_vcf();
    enable_disable_btn_download_f_vcf();

    //NON FILTERED VCFs
    /**
     * btnAddnonfiltered, on click, move the selected files of select_nonfilteredvcf_list to select_nonfilteredvcf_list_selected
     */
    $("#btnAddnonfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_nonfilteredvcf_list")[0].length - 1; i >= 0; i--) {
            if ($("#select_nonfilteredvcf_list")[0][i].selected) {
                $("#select_nonfilteredvcf_list")[0][i].selected = false;
                $("#select_nonfilteredvcf_list_selected").append($("#select_nonfilteredvcf_list")[0][i]);
            }
        }
        enable_disable_btn_download_nf_vcf();
    });

    /**
     * btnRemovenonfiltered, on click, move the selected files of select_nonfilteredvcf_list_selected to select_nonfilteredvcf_list
     */
    $("#btnRemovenonfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_nonfilteredvcf_list_selected")[0].length - 1; i >= 0; i--) {
            if ($("#select_nonfilteredvcf_list_selected")[0][i].selected) {
                $("#select_nonfilteredvcf_list_selected")[0][i].selected = false;
                $("#select_nonfilteredvcf_list").append($("#select_nonfilteredvcf_list_selected")[0][i]);
            }
        }
        enable_disable_btn_download_nf_vcf();
    });

    /**
     * btnSelectAllnonfiltered, on click, move all files of select_nonfilteredvcf_list to select_nonfilteredvcf_list_selected
     */
    $("#btnSelectAllnonfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_nonfilteredvcf_list")[0].length - 1; i >= 0; i--) {
            $("#select_nonfilteredvcf_list")[0][i].selected = false;
            $("#select_nonfilteredvcf_list_selected").append($("#select_nonfilteredvcf_list")[0][i]);
        }
        enable_disable_btn_download_nf_vcf();
    });

    /**
     * btnUnselectAllnonfiltered, on click, move all files of select_nonfilteredvcf_list_selected to select_nonfilteredvcf_list
     */
    $("#btnUnselectAllnonfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_nonfilteredvcf_list_selected")[0].length - 1; i >= 0; i--) {
            $("#select_nonfilteredvcf_list_selected")[0][i].selected = false;
            $("#select_nonfilteredvcf_list").append($("#select_nonfilteredvcf_list_selected")[0][i]);
        }
        enable_disable_btn_download_nf_vcf();
    });


    //FILTERED VCFs
    /**
     * btnAddfiltered, on click, move the selected files of select_filteredvcf_list to select_filteredvcf_list_selected
     */
    $("#btnAddfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_filteredvcf_list")[0].length - 1; i >= 0; i--) {
            if ($("#select_filteredvcf_list")[0][i].selected) {
                $("#select_filteredvcf_list")[0][i].selected = false;
                $("#select_filteredvcf_list_selected").append($("#select_filteredvcf_list")[0][i]);
            }
        }
        enable_disable_btn_download_f_vcf();
    });

    /**
     * btnRemovefiltered, on click, move the selected files of select_filteredvcf_list_selected to select_filteredvcf_list
     */
    $("#btnRemovefiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_filteredvcf_list_selected")[0].length - 1; i >= 0; i--) {
            if ($("#select_filteredvcf_list_selected")[0][i].selected) {
                $("#select_filteredvcf_list_selected")[0][i].selected = false;
                $("#select_filteredvcf_list").append($("#select_filteredvcf_list_selected")[0][i]);
            }
        }
        enable_disable_btn_download_f_vcf();
    });

    /**
     * btnSelectAllfiltered, on click, move all files of select_filteredvcf_list to select_filteredvcf_list_selected
     */
    $("#btnSelectAllfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_filteredvcf_list")[0].length - 1; i >= 0; i--) {
            $("#select_filteredvcf_list")[0][i].selected = false;
            $("#select_filteredvcf_list_selected").append($("#select_filteredvcf_list")[0][i]);
        }
        enable_disable_btn_download_f_vcf();
    });

    /**
     * btnUnselectAllfiltered, on click, move all files of select_filteredvcf_list_selected to select_filteredvcf_list
     */
    $("#btnUnselectAllfiltered").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_filteredvcf_list_selected")[0].length - 1; i >= 0; i--) {
            $("#select_filteredvcf_list_selected")[0][i].selected = false;
            $("#select_filteredvcf_list").append($("#select_filteredvcf_list_selected")[0][i]);
        }
        enable_disable_btn_download_f_vcf();
    });
});

/**
 * @name enable_disable_btn_download_nf_vcf()
 * @description enable/disable button download nonfiltered vcf if nothing to download
 */
function enable_disable_btn_download_nf_vcf(){
    if ($("#select_nonfilteredvcf_list_selected")[0].length == 0){
        $("#btn_download_nf_vcf").prop('disabled', true);
    }else{
        $("#btn_download_nf_vcf").prop('disabled', false);
    }
}

/**
 * @name enable_disable_btn_download_f_vcf()
 * @description enable/disable button download filtered vcf if nothing to download
 */
function enable_disable_btn_download_f_vcf(){
    if ($("#select_filteredvcf_list_selected")[0].length == 0){
        $("#btn_download_f_vcf").prop('disabled', true);
    }else{
        $("#btn_download_f_vcf").prop('disabled', false);
    }
}