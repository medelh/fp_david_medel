/*Javascript associated to BAM.html */
$(document).ready(function () {

    enable_disable_btn_download();

    /**
     * btnAdd, on click, move the selected files of select_bam_list to select_bam_list_selected
     */
    $("#btnAdd").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_bam_list")[0].length - 1; i >= 0; i--) {
            if ($("#select_bam_list")[0][i].selected) {
                $("#select_bam_list")[0][i].selected = false;
                $("#select_bam_list_selected").append($("#select_bam_list")[0][i]);
            }
        };
        enable_disable_btn_download();
    });

    /**
     * btnRemove, on click, move the selected files of select_bam_list_selected to select_bam_list
     */
    $("#btnRemove").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_bam_list_selected")[0].length - 1; i >= 0; i--) {
            if ($("#select_bam_list_selected")[0][i].selected) {
                $("#select_bam_list_selected")[0][i].selected = false;
                $("#select_bam_list").append($("#select_bam_list_selected")[0][i]);
            }
        };
        enable_disable_btn_download();
    });

    /**
     * btnSelectAll, on click, move all files of select_bam_list to select_bam_list_selected
     */
    $("#btnSelectAll").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_bam_list")[0].length - 1; i >= 0; i--) {
            $("#select_bam_list")[0][i].selected = false;
            $("#select_bam_list_selected").append($("#select_bam_list")[0][i]);
        };
        enable_disable_btn_download();
    });

    /**
     * btnUnselectAll, on click, move all files of select_bam_list_selected to select_bam_list
     */
    $("#btnUnselectAll").click(function (event) {
        event.preventDefault();
        for (let i = $("#select_bam_list_selected")[0].length - 1; i >= 0; i--) {
            $("#select_bam_list_selected")[0][i].selected = false;
            $("#select_bam_list").append($("#select_bam_list_selected")[0][i]);
        };
        enable_disable_btn_download();
    })
});

/**
 * @name enable_disable_btn_submit()
 * @description enable/disable button download bams if nothing to download
 */
function enable_disable_btn_download(){
    if ($("#select_bam_list_selected")[0].length == 0){
        $("#btn_download_bam").prop('disabled', true);
    }else{
        $("#btn_download_bam").prop('disabled', false);
    }
}