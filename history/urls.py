'''This file contains all the urls in history'''
from django.urls import re_path
from . import views

urlpatterns = [
    re_path(r'^projects', views.history),
    re_path(r'^multiQC', views.multiqc),
    re_path(r'^BAM', views.bams),
    re_path(r'^VCF', views.vcfs),
]
