'''This file contains all the views of projects_manager_app'''
#django imports
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

# Create your views here.
def init(request):
    'init view redirects to /home/'
    return redirect('/home/')

@login_required(login_url="/user_management/login/")
def home(request):
    'home view render the projects_manager_app/home.html'
    args = {}
    return render(request, "projects_manager_app/home.html", args)
