'''This file contains all the urls and suburls of the project'''
from django.contrib import admin
from django.urls import path, include
from . import views

"""projects_manager_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.init),
    path('home/', views.home),
    path('user_management/', include('user_management.urls')),
    path('performQCrun/', include('perform_QC_run.urls')),
    path('history/', include('history.urls')),
]
